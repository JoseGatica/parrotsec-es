<?php require(__DIR__ . '/include/parrot.inc.php'); ?>
<section class="bg-primary" id="one">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-12"><h2 class="margin-top-0 text-primary wow fadeInDown"><?php echo $lang['download-other-1']; ?></h2><br/><br/></div>
            </div>
            <div class="row">
                <div class="col-lg-6 text-center">
                    <p class="text-faded wow fadeInRight"><?php echo $lang['download-other-2']; ?></p>
                    <p class="text-faded wow fadeInUp"><?php echo $lang['download-other-3']; ?></p>
                    <p class="text-faded wow fadeInLeft"><?php echo $lang['download-other-4']; ?>
                    </p>
                </div>
                <div class="col-lg-6 text-center">
                    <p><img class="rounded mx-auto d-block float-left" style="width:500px" src="img/arm-soc.jpg" /></p>
                </div><br/><br/><br/>
            </div>
            <div class="row">

              <div class="col-lg-4 col-md-4 text-center">
                <div class="panel download panel-blue">
                  <div class="panel-heading arrow_box text-center">
                    <h3><?php echo $lang['download-other-19']; ?></h3>
                  </div>
                  <div class="panel-body text-center">
                    <p class="lead" style="font-size:40px"><strong><?php echo $lang['download-other-20']; ?></strong></p>
                  </div>
                  <ul class="list-group list-group-flush text-center">
      							<li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-21']; ?></li>
                    <li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-22']; ?></li>
      						</ul>
      						<div class="panel-footer">
      							<a class="btn btn-lg btn-block btn-info" href="https://cdimage.parrotsec.org/parrot/iso/arm/rpi"><?php echo $lang['download-other-23']; ?></a>
      						</div>
      					</div>
              </div>

              <div class="col-lg-4 col-md-4 text-center">
                <div class="panel download panel-blue">
                  <div class="panel-heading arrow_box text-center">
                    <h3><?php echo $lang['download-other-24']; ?></h3>
                  </div>
                  <div class="panel-body text-center">
                    <p class="lead" style="font-size:40px"><strong><?php echo $lang['download-other-20']; ?></strong></p>
                  </div>
                  <ul class="list-group list-group-flush text-center">
      							<li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-25']; ?></li>
                    <li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-26']; ?></li>
                    <li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-27']; ?></li>
      						</ul>
      						<div class="panel-footer">
      							<a class="btn btn-lg btn-block btn-info" href="https://cdimage.parrotsec.org/parrot/iso/arm/opi"><?php echo $lang['download-other-23']; ?></a>
      						</div>
      					</div>
              </div>

              <div class="col-lg-4 col-md-4 text-center">
                <div class="panel download panel-blue">
                  <div class="panel-heading arrow_box text-center">
                    <h3><?php echo $lang['download-other-28']; ?></h3>
                  </div>
                  <div class="panel-body text-center">
                    <p class="lead" style="font-size:40px"><strong><?php echo $lang['download-other-20']; ?></strong></p>
                  </div>
                  <ul class="list-group list-group-flush text-center">
      							<li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-29']; ?></li>
                    <li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-28']; ?></li>
                    <li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-30']; ?></li>
                    <li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-31']; ?></li>
                    <li class="list-group-item"><i class="icon-ok text-info"></i><?php echo $lang['download-other-32']; ?></li>
      						</ul>
      						<div class="panel-footer">
      							<a class="btn btn-lg btn-block btn-info" href="https://cdimage.parrotsec.org/parrot/iso/arm/pine64"><?php echo $lang['download-other-23']; ?></a>
      						</div>
      					</div>
              </div>
              
              </div>
          </div>

          </div>
        </section>
<?php require(__DIR__ . '/include/foot.php'); ?>
