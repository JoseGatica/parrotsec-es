<?php $page="download"; ?>
<html>
<head>
  <title>VideoTutoriales</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="author"   content="Lorenzo Faletra - José Gatica - Claudio Peon">
  <!-- Meta data to help Facebook generate the right advertisement -->
  <meta property="og:type"        content="article" />
  <meta property="og:title"       content="Videotutoriales ParrotSec en Español - Esperemos ansiosos el lanzamiento de "Migración", la segunda edición de nuestra documentacin oficial." />
  <meta property="og:site_name"   content="Parrot Security"/>
  <meta property="og:url"         content="https://www.parrotsec-es.org" />
  <meta property="og:image"       content="https://www.parrotsec.org/img/facebook-preview.png" />
  <meta property="og:description" content="Completa serie de videotutoriales de hacking, desde cero. Descubre nuestro increíble entorno GNU/Linux de ciber seguridad. Incluye un laboratorio portable completo para expertos forenses digitales y de seguridad, pero también incluye todo lo que necesitas para desarrollar tus propios programas o proteger tu privacidad con anonimato y crypto-herramientas." />
  <meta property="fb:app_id"      content="">
  <link href="css/parrot.min.css" type="text/css" rel="stylesheet" />
  <!--external libraries CSS -->
  <link href="css/vendor.min.css"   type="text/css" rel="stylesheet" />
  <!--custom + external libraries JS -->
  <!--<script src="js/main.min.js"      type="text/javascript"></script>-->
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <link rel="shortcut icon" href="img/logo.png">
</head>
<body>
  <div class="finger-detection-open-menu visible-xs"></div>
  <header>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-2 logo">
          <a href="#">
            <img src="img/logo.png" class="wow fadeIn smoothScroll" data-linkto='parrot-os' id="parrotLogo"/>
          </a>
          <i class="fa fa-bars fa-lg pull-left visible-xs" id="open_main_menu"></i>
        </div>
        <nav class="hidden-xs col-sm-10 text-right wow fadeIn" id="main-menu">
          <ul>
            <li>
              <a class='smoothScroll' id="home" data-linkto='parrot-os' href="https://www.parrotsec-es.org/">Inicio</a>
            </li>
            <li>
              <a class='smoothScroll' data-linkto='parrot-download' href="https://www.parrotsec-es.org/download.php">Descargas</a>
            </li>
            <li>
              <a class='smoothScroll' data-linkto='parrot-videos' href="https://www.parrotsec-es.org/videos.php">Videotutoriales</a>
            </li>
            <li>
              <a class='smoothScroll' data-linkto='parrot-news' href="https://blog.parrotsec.org" target="blank">Noticias</a>
            </li>
            <li>
              <a class='smoothScroll' data-linkto='parrot-documentation' href="https://docs.parrotsec-es.org/" target="blank">Documentación</a>
            </li>
            <li>
              <a class='smoothScroll' data-linkto='parrot-community' href="https://community.parrotsec.org/viewforum.php?id=25" target="blank">Comunidad</a>
            </li>
            <li>
              <a class='smoothScroll' data-linkto='parrot-faq' href="https://github.com/josegatica/parrot-docu-es/blob/master/00.-%20FAQ.md" target="blank">F.A.Q.</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
</header>
	<body class="mdl-parrot mdl-color--grey-100 mdl-color-text--grey-700 mdl-base">
		<center>
			<div style="padding-top: 50px; oadding-bottom: 50px;">
			<hr>
			<br>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/6AkhIOCvmYQ?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/9_BZw2n7MSU?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/TBGUf3oSjvI?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/3LD6-6It_5Q?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/f9SdFAJuyPo?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/WwIiI398kmE?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/afuPepqpCpk?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/1HcGUP90eMU?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/JvSDU5ZqVs8?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/F6X_4jJn3OA?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/1527EPck4NY?rel=0" frameborder="0" allowfullscreen></iframe>
			<br>
			<hr>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/t7Nx3q00XjQ?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
		</center>
	</body>
</html>
