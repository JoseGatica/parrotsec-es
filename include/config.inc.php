<?php

const CONFIG_SITE_NAME = 'Proyecto Parrot';
const CONFIG_SITE_SLOGAN = 'La mejor elección para expertos en Seguridad, Desarrolladores y Cripto-Adictos.';
const CONFIG_SITE_DESCRIPTION = 'Descubre nuestro increíble entorno GNU/Linux de ciber seguridad. Incluye un completo laboratorio portátil para expertos forenses y digitales, pero también incluye todo lo que necesitas para desarrollar tus propios programas o proteger tu privacidad con herramientas criptográficas y de anonimato.';
const CONFIG_SITE_URL = 'https://www.parrotsec-es.org/';
const CONFIG_SITE_URL_STATIC_RESOURCES = 'https://www.parrotsec-es.org/';
const CONFIG_SITE_AUTOR = 'Lorenzo Faletra - José Gatica';
const CONFIG_SITE_LANG = 'es';
const CONFIG_SYSTEM_VERSION = '4.0.1';

const PARROT_FULL_SIZE_64 = '3.6GB';
const PARROT_FULL_SIZE_32 = '3.7GB';

const PARROT_LITE_SIZE_64 = '1.56GB';
const PARROT_LITE_SIZE_32 = '1.58GB';

?>
