<?PHP
$lang = array();
$lang['nav-1']			= 'Toggle navigation';
$lang['nav-2']			= 'Parrot';
$lang['nav-3']			= 'Descargas';
$lang['nav-4']			= 'Noticias';
$lang['nav-5']			= 'Wiki';
$lang['nav-6']			= 'Dev';
$lang['nav-7']			= 'Comunidad';
$lang['nav-8']			= 'Partners';
$lang['nav-9']			= 'Equipo';
$lang['nav-10']			= 'Donar';
$lang['nav-11']			= 'F.A.Q.';
$lang['nav-12']			= 'Editar';
$lang['nav-13']			= 'Continuar';
$lang['nav-14']			= 'Partners';
$lang['nav-15']			= 'Equipo';
$lang['nav-16']			= 'Idioma';

$lang['index-1']			= 'Descubre el <b>Universo Parrot</b> y obtén lo mejor de nuestra increíble plataforma basada en Debian.';
$lang['index-2']			= 'Herramientas profesionales para pruebas de seguridad, desarrollo de software y defensa de la privacidad, todo en un solo lugar.';
$lang['index-3']			= 'Descargas';
$lang['index-4']			= 'Descubre';
$lang['index-5']			= 'Sistema Parrot';
$lang['index-6']			= 'Parrot es una distribución GNU/Linux basada en Debian Testing y diseñada pensando en la Seguridad, Desarrollo de Software y la Privacidad.';
$lang['index-7']			= 'It includes a full portable laboratory for security and digital forensics experts, but it also includes all you need to develop your own software or protect your privacy while surfing the net.';
$lang['index-8']			= 'Continuar';
$lang['index-9']			= 'Project Goals';
$lang['index-10']			= 'Seguridad';
$lang['index-11']			= 'Un completo arsenal de <b>herramientas de seguridad</b> justo en la palma de tu mano.';
$lang['index-12']			= 'Privacidad';
$lang['index-13']			= 'Un sistema seguro y aislado, listo para surfear y comunicarte secretamente.';
$lang['index-14']			= 'Desarrollo';
$lang['index-15']			= 'Una completa gama para desarrollo con los mejores editores, lenguajes y tecnologías "out of the box".';
$lang['index-16']			= 'Características';
$lang['index-17']			= 'Seguro';
$lang['index-18']			= 'Siempre actualizado, lanzamientos frecuentes y copmletamente <b>aislados</b>! Todo está bajo tu completo control.';
$lang['index-19']			= 'Free (como en libertad -freedom-)';
$lang['index-20']			= 'Siéntete libre de adquirir el sistema operativo, compartir con quien quieras, leer el código fuente y cambiarlo como quieras!<br /> Este sistema operativo está hecho para <b>respetar tu libertad</b>, y siempre será así.';
$lang['index-21']			= 'Liviano';
$lang['index-22']			= 'Nos preocupamos por el consumo de recursos, y este sistema operativo ha probado ser  <b>extremadamente liviano</b> y corre sorprendentemente rápido incluso en equipos muy antiguos o con recursos muy limitados.';
$lang['index-23']			= 'Escritorio';
$lang['index-24']			= 'Menú principal';
$lang['index-25']			= 'Manú de Internet';
$lang['index-26']			= 'Manú inferior y terminal';
$lang['index-27']			= 'Herramientas de terminal';
$lang['index-28']			= 'Faraday IDE';
$lang['index-29']			= 'Anonsurf y firejail';
$lang['index-30']			= 'Ricochet, torbrowser, zulucrypt and sirikali';
$lang['index-31']			= 'QtCreator';

$lang['foot-1']				= 'Acerca de';
$lang['foot-2']				= 'Términos y Condiciones';
$lang['foot-3']				= 'Partners';
$lang['foot-4']				= 'Desarrolladores';
$lang['foot-5']				= 'Wikipedia';
$lang['foot-6']				= 'Distrowatch';
$lang['foot-7']				= 'Softpedia';
$lang['foot-8']				= 'Censo de Derivados de Debian';
$lang['foot-9']				= 'Información';
$lang['foot-10']			= 'Se dice de nosotros';
$lang['foot-11']			= 'Síguenos';

$lang['download-1']			= 'Descargar Parrot GNU/Linux';
$lang['download-2']			= '<b>Security</b>';
$lang['download-3']			= 'Parrot Security es nuestro completo entorno "todo en uno" para pentesting, privacidad, forenses digitales, ingeniería inversa y desarrollo de software.';
$lang['download-4']			= '<b>Home/Workstation</b>';
$lang['download-5']			= 'Parrot Home es un sistema operativo muy liviano para <b>uso diario</b> y <b>protección de la privacidad</b>. Está diseñado para usuarios casuales que aman la interfaz de Parrot.';
$lang['download-6']			= '<b>Otras versiones</b>';
$lang['download-7']			= 'The Parrot Project releases other images of the Parrot System specifically designed for special use cases or uncommon devices. Here you can find the ARM images and other custom editions.';
$lang['download-8']			= 'Etcher Image Writer';
$lang['download-9']			= 'Archivo Parrot';
$lang['download-10']		= '¿Necesitas ayuda? ¡Contacta a nuestra comunidad!';
$lang['download-11']		= 'Tenemos una gran comunidad internacional y muchos grupos locales con gente que quiere ayudarte';
$lang['download-12']		= 'Comunidad';
$lang['download-13']		= '¿Necesitas otras versiones?';
$lang['download-14']		= 'Recuerda que las imágenes en Beta son experimentales</b><br /> y algunas características podrían no funcionar apropiadamente';
$lang['download-15']		= 'Todas las ISOs';
$lang['download-16']		= 'tabla de Características';
$lang['download-17']		= 'características';
$lang['download-18']		= 'Security';
$lang['download-19']		= 'Home';
$lang['download-20']		= 'IoT';
$lang['download-21']		= 'Arquitectura';
$lang['download-22']		= 'x86 32bit &amp; 64bit';
$lang['download-23']		= 'x86 32bit &amp; 64bit';
$lang['download-24']		= 'ARMv7 &amp; ARMv8 64bit';
$lang['download-25']		= 'versión de kernel Linux';
$lang['download-26']		= '4.16';
$lang['download-27']		= '(depende de la placa)';
$lang['download-28']		= 'Versión de Debian';
$lang['download-29']		= 'Debian 10 Buster';
$lang['download-30']		= 'Modelo de lanzamientos';
$lang['download-31']		= 'Rolling Release';
$lang['download-32']		= 'Entorno de escritorio por defecto';
$lang['download-33']		= 'MATE 1.20';
$lang['download-34']		= 'Sandbox';
$lang['download-35']		= 'Suite de Oficina';
$lang['download-36']		= 'LibreOffice 6.x';
$lang['download-37']		= 'Abiword/Gnumeric';
$lang['download-38']		= 'Suite para Web';
$lang['download-39']		= 'Firefox 60';
$lang['download-40']		= 'Epiphany (puede incluir firefox-esr)';
$lang['download-41']		= 'Bloqueadores de publicidad y protección web';
$lang['download-42']		= 'Ublock Origin + NoScript + Privacy Badger';
$lang['download-43']		= 'AnonSurf + TOR Browser';
$lang['download-44']		= 'Herramientas de Criptografía';
$lang['download-45']		= 'Herramientas de Pentesting y Seguridad';
$lang['download-46']		= ' (Pack esencial)';
$lang['download-47']		= 'Herramientas de Ingeniería Inversa';
$lang['download-48']		= 'Herramientas forenses';
$lang['download-49']		= 'Herramientas de Desarrollo';
$lang['download-50']		= 'Bitcoin Wallet (electrum)';
$lang['download-51']		= 'Soporte para WINE';
$lang['download-52']		= 'Soporte para UEFI';
$lang['download-53']		= 'MATE 1.20 or headless';
$lang['download-54']		= ' (cloning tools incluídos)';
$lang['download-55']		= ' (entorno completo)';


$lang['download-security-1']	= 'Parrot Security ';
$lang['download-security-2']	= 'Parrot Security es nuestro completo entorno "todo en uno" para pentesting, privacidad, forenses digitales, ingeniería inversa y desarrollo de software.';
$lang['download-security-3']	= 'El sistema operativo incluye un arsenal completo de herramientas orientadas a la seguridad para cubrir muchas categorías del trabajo de un pentester.';
$lang['download-security-4']	= 'Security Edition';
$lang['download-security-5']	= ' Para computadores modernos';
$lang['download-security-6']	= 'interfaz: MATE';
$lang['download-security-7']	= 'Formato: ISO Hybrid';
$lang['download-security-8']	= 'Tamaño: ';
$lang['download-security-9']	= 'Mirrors';
$lang['download-security-10']	= 'Torrent';
$lang['download-security-11']	= 'Signed Hashes';
$lang['download-security-12']	= ' Para computadores más antiguos';


$lang['download-home-1']	= 'Parrot Home ';
$lang['download-home-2']	= 'Parrot Home es una edición especial de Parrot diseñada para uso cotidiano, y está orientado a usuarios regulares que necesitan un sistema operativo liviano, siempre actualizado y agradable a la vista en sus notebook o estaciones de trabajo.';
$lang['download-home-3']	= 'Esta distribución tiene la misma interfaz de un entorno regular de Parrot e incluye todos los programas básicos para el trabajo diario. Parrot Home también incluye programas para chatear privadamente con la gente, encriptar documentos con los más altos estándares criptográficos o navegar por la web de manera completamente anónima y segura.';
$lang['download-home-4']	= 'Este sistema también puede ser usado como punto de partida para construir una plataforma personalizada de pentesting sólo con las herramientas que necesitas, o lo puedes usar para construir tu estación de trabajo personal tomando ventaja de todas las más recientes y poderosas tecnologías de Debian sin problema alguno.';
$lang['download-home-5']	= 'Home Edition';
$lang['download-home-6']	= ' Para computadores modernos';
$lang['download-home-7']	= 'Interfaz: MATE';
$lang['download-home-8']	= 'Formato: ISO Hybrid';
$lang['download-home-9']	= 'Tamaño: ';
$lang['download-home-10']	= 'Mirrors';
$lang['download-home-11']	= 'Torrent';
$lang['download-home-12']	= 'Signed Hashes';
$lang['download-home-13']	= ' Para computadores más antiguos';


$lang['download-other-1']	= 'versiones especiales de Parrot y Derivados Oficiales';
$lang['download-other-2']	= 'En esta página puedes descargar las versiones oficiales de Parrot para placas integradas ARM y otras versiones personalizadas de Parrot para otras plataformas especiales.';
$lang['download-other-3']	= 'Las versiones ARM en esta página son imágenes oficiales lanzadas por nuestro propio equipo ARM.';
$lang['download-other-4']	= 'Aquí también puedes encontrar derivados personalizados de Parrot y versiones experimentales con configuraciones personalizadas para dispositivos especiales, o también derivados de Parrot propuestos por nuestra comunidad y desarrollados en nuestra plataforma oficial.';
$lang['download-other-5']	= 'Parrot Pico';
$lang['download-other-6']	= 'Arquitectura: ';
$lang['download-other-7']	= 'Tamaño: ';
$lang['download-other-8']	= 'Interfaz: ';
$lang['download-other-9']	= 'Estilo: Minimal';
$lang['download-other-10']	= 'Formato: ISO Hybrid';
$lang['download-other-11']	= 'Descargar';
$lang['download-other-12']	= 'Mirrors';
$lang['download-other-13']	= 'Torrent';
$lang['download-other-14']	= 'Signed Hashes';
$lang['download-other-15']	= 'Código Fuente de Parrot';
$lang['download-other-16']	= 'fuente';
$lang['download-other-17']	= 'Código fuente de Parrot Security';
$lang['download-other-18']	= 'Formato: ';
$lang['download-other-19']	= 'Rpi';
$lang['download-other-20']	= 'arm';
$lang['download-other-21']	= 'armhf (armv7) 32bit';
$lang['download-other-22']	= 'Raspberry Pi 2 y 3';
$lang['download-other-23']	= 'Descargar (experimental)';
$lang['download-other-24']	= 'Orange Pi';
$lang['download-other-25']	= 'armhf/arm64 (arm v7/v8) 32/64bit';
$lang['download-other-26']	= 'Orange Pi PC';
$lang['download-other-27']	= 'Orange Pi Nano';
$lang['download-other-28']	= 'Pine64';
$lang['download-other-29']	= 'arm64 (armv8) 64bit';
$lang['download-other-30']	= 'Rock64';
$lang['download-other-31']	= 'Sopine';
$lang['download-other-32']	= 'PineBook';

?>
